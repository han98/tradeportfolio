package com.citi.spring.PortfolioApi;

import java.util.Date;

import com.citi.spring.PortfolioApi.entities.Bond;
import com.citi.spring.PortfolioApi.entities.Commodity;
import com.citi.spring.PortfolioApi.entities.Equity;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class PortfolioApiApplicationTests {

	@Test
	void contextLoads() {
	}


	@Test public void testEquity() {
		//arrange
		Date dateCreatedTest = new Date();
		String tickerTest = "test";
		int quantTest = 4;
		String amountTest = "50";
		
		//act
		Equity test = new Equity(dateCreatedTest, tickerTest, quantTest, amountTest);
		
		//assert
		assert(test.getDateCreated().equals(dateCreatedTest));
		assert(test.getTicker().equals(tickerTest));
		assert(test.getQuantity() == quantTest);
		assert(test.getAmount().equals(amountTest));
		
	}

	@Test public void testCommodity() {
		//arrange
		Date dateCreatedTest = new Date();
		String tickerTest = "test";
		int quantTest = 4;
		double priceTest = 6;
		
		//act
		Commodity test = new Commodity(dateCreatedTest, tickerTest, quantTest, priceTest);
		
		//assert
		assert(test.getDateCreated().equals(dateCreatedTest));
		assert(test.getTicker().equals(tickerTest));
		assert(test.getQuantity() == quantTest);
		assert(test.getPrice() == priceTest);
		
	}

	@Test public void testBond() {
		//arrange
		Date dateCreatedTest = new Date();
		Date maturityDateTest = new Date();
		String nameTest = "testName";
		double faceValueTest = 4.4;
		int quantTest = 4;
		int yieldTest = 6;
		double couponTest = 3.4;
		
		//act
		Bond test = new Bond(dateCreatedTest, maturityDateTest, nameTest, faceValueTest, quantTest, yieldTest, couponTest);
		
		//assert
		assert(test.getDateCreated().equals(dateCreatedTest));
		assert(test.getMaturityDate().equals(maturityDateTest));
		assert(test.getName().equals(nameTest));
		assert(test.getFaceValue()== faceValueTest);
		assert(test.getQuantity() == quantTest);
		assert(test.getYield() == yieldTest);
		assert(test.getCoupon() == couponTest);
		
	}
		
	
	@Test public void testCommodityDateCreated(){
		Date dateCreated = new Date();
		Commodity test = new Commodity();
		test.setDateCreated(dateCreated);
		Date testDate = test.getDateCreated();
		assert dateCreated.equals(testDate);
		}

	@Test public void testEquityDateCreated(){
		Date dateCreated = new Date();
		Equity test = new Equity();
		test.setDateCreated(dateCreated);
		Date testDate = test.getDateCreated();
		assert dateCreated.equals(testDate);
	}

	@Test public void testBondDateCreated(){
	Date dateCreated = new Date();
	Bond test = new Bond();
	test.setDateCreated(dateCreated);
	Date testDate = test.getDateCreated();
	assert dateCreated.equals(testDate);
	}

	@Test public void testMaturityDate(){
		Date maturityDate = new Date();
		Bond test = new Bond();
		test.setMaturityDate(maturityDate);
		Date testDate = test.getMaturityDate();
		assert maturityDate.equals(testDate);
		}

	
	@Test public void testEqTicker() {
		//Equity
		String giveAnotherTicker = "baked beans";
		//Given
		Equity test = new Equity();
		test.setTicker(giveAnotherTicker);
		//When
		String anotherTestTicker = test.getTicker();
		//Then
		assert giveAnotherTicker.equals(anotherTestTicker);
	}

	@Test public void testComTicker(){
		//Commodity
		//Setup
		String giveTicker = "beans";
		//Given
		Commodity test = new Commodity();
		test.setTicker(giveTicker);
		//When
		String testTicker = test.getTicker();
		//Then
		assert giveTicker.equals(testTicker);
	}

	@Test public void testComState() {
		//Commodity
		//Setup
		String giveState = "CREATED";
		//Given
		Commodity test = new Commodity();
		test.setState(giveState);
		//When
		String testState = test.getState();
		//Then
		assert giveState.equals(testState);

	}

	@Test public void testBondState(){
		//Bond
		String giveBondState = "Heinz baked beans";
		//Given
		Bond test = new Bond();
		test.setState(giveBondState);
		//When
		String bondTestState = test.getState();
		//Then
		assert giveBondState.equals(bondTestState);
	}

	@Test public void testEqState(){
		//Equity
		String giveAnotherState = "baked beans";
		//Given
		Equity test = new Equity();
		test.setEquityStatus(giveAnotherState);
		//When
		String anotherTestState = test.getEquityStatus();
		//Then
		assert giveAnotherState.equals(anotherTestState);
	}

	@Test public void testName() {
		//Setup
		String giveName = "Tyler";
		//Given
		Bond test = new Bond();
		test.setName(giveName);
		//When
		String testName = test.getName();
		//Then
		assert giveName.equals(testName);
	}

	@Test public void testQuantity() {
		//Commodity
		//Setup
		int giveQuant = 4;
		//Given
		Commodity test = new Commodity();
		test.setQuantity(giveQuant);
		//When
		int testQuant = test.getQuantity();
		//Then
		assert giveQuant == testQuant;

	}

	@Test public void testBondQuant(){
		//Bond
		int giveQuant3 = 1;
		//Given
		Bond test = new Bond();
		test.setQuantity(giveQuant3);
		//When
		int bondTestQuant = test.getQuantity();
		//Then
		assert giveQuant3 == bondTestQuant;
	}

	@Test public void testEqQuant(){
		//Equity
		int giveQuant2 = 2;
		//Given
		Equity test = new Equity();
		test.setQuantity(giveQuant2);
		//When
		int anotherTestQuant = test.getQuantity();
		//Then
		assert giveQuant2 == anotherTestQuant;
	}

	@Test public void testAmount() {
		//Equity
		String giveAmount = "300";
		//Given
		Equity test = new Equity();
		test.setAmount(giveAmount);
		//When
		String testAmount = test.getAmount();
		//Then
		assert giveAmount.equals(testAmount);
	}

	@Test public void testPrice(){
		//Commodity
		//Setup
		double givePrice = 20.01;
		//Given
		Commodity test = new Commodity();
		test.setPrice(givePrice);
		//When
		double testPrice = test.getPrice();
		//Then
		assert givePrice == testPrice;
	}

	@Test public void testCoupon(){
		//Commodity
		//Setup
		double giveCoupon = 20.01;
		//Given
		Bond test = new Bond();
		test.setCoupon(giveCoupon);
		//When
		double testCoupon = test.getCoupon();
		//Then
		assert giveCoupon == testCoupon;
	}

	@Test public void testFaceValue(){
		//Commodity
		//Setup
		double giveFace = 20.01;
		//Given
		Bond test = new Bond();
		test.setFaceValue(giveFace);
		//When
		double testFace = test.getFaceValue();
		//Then
		assert giveFace == testFace;
	}


	@Test public void testYield(){
		//Bond
		int giveYield = 1;
		//Given
		Bond bondYield = new Bond();
		bondYield.setYield(giveYield);
		//When
		int bondTest = bondYield.getYield();
		//Then
		assert giveYield == bondTest;
	}
}
