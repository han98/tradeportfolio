package com.citi.spring.PortfolioApi.rest;

import java.util.Collection;

import java.util.Optional;
import com.citi.spring.PortfolioApi.entities.Commodity;
import com.citi.spring.PortfolioApi.service.CommodityService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/portfolio/commodity")
@CrossOrigin
public class CommodityController {

    @Autowired
    private CommodityService commodityService;

    @RequestMapping(method=RequestMethod.GET)
    public Collection<Commodity> getAllCommodities(){
        return commodityService.getAll();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    //Optional keyword for an object that may or may not be null. E.g. if there retrieve
    public Optional<Commodity> getCommodityById(@PathVariable("id") String id) {
        return commodityService.getCommodityById(new ObjectId(id));
	}

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public void deleteCommodity(@PathVariable("id") String id) {
        commodityService.deleteCommodity(new ObjectId(""+id));
    }

    @RequestMapping(method=RequestMethod.POST)
    public void createCommodity(@RequestBody Commodity c){
        commodityService.createCommodity(c);
    }

    @RequestMapping(method=RequestMethod.PUT, value = "/{id}")
    public void updateCommodity(@PathVariable("id") ObjectId id,@RequestBody Commodity c) {
        commodityService.updateCommodity(id, c);
    }
    
}
