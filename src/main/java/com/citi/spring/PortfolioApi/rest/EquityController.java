package com.citi.spring.PortfolioApi.rest;

import java.util.Collection;

import java.util.Optional;
import com.citi.spring.PortfolioApi.entities.Equity;
import com.citi.spring.PortfolioApi.service.EquityService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/portfolio/equity")
@CrossOrigin
public class EquityController {
    
    @Autowired
    private EquityService equityService;

    @RequestMapping(method=RequestMethod.GET)
    public Collection<Equity> getAllTrades(){
        return equityService.getAll();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    //Optional keyword for an object that may or may not be null. E.g. if there retrieve
    public Optional<Equity> getEquityById(@PathVariable("id") String id) {
        return equityService.getEquityById(new ObjectId(id));
	}

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public void deleteEquity(@PathVariable("id") String id) {
        equityService.deleteEquity(new ObjectId(""+id));
    }

    @RequestMapping(method=RequestMethod.POST)
    public void createEquity(@RequestBody Equity e){
        equityService.createEquity(e);
    }

    @RequestMapping(method=RequestMethod.PUT, value = "/{id}")
    public void updateEquity(@PathVariable("id") ObjectId id,@RequestBody Equity e) {
        equityService.updateEquity(id, e);
    }

}