package com.citi.spring.PortfolioApi.rest;

import java.util.Collection;

import java.util.Optional;
import com.citi.spring.PortfolioApi.entities.Bond;
import com.citi.spring.PortfolioApi.service.BondService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/portfolio/bond")
@CrossOrigin
public class BondController {

    @Autowired
    private BondService bondService;

    @RequestMapping(method=RequestMethod.GET)
    public Collection<Bond> getAllBonds(){
        return bondService.getAll();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    //Optional keyword for an object that may or may not be null. E.g. if there retrieve
    public Optional<Bond> getBondById(@PathVariable("id") String id) {
        return bondService.getBondById(new ObjectId(id));
	}

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public void deleteBond(@PathVariable("id") String id) {
        bondService.deleteBond(new ObjectId(""+id));
    }

    @RequestMapping(method=RequestMethod.POST)
    public void createBond(@RequestBody Bond b){
        bondService.createBond(b);
    }

    @RequestMapping(method=RequestMethod.PUT, value = "/{id}")
    public void updateBond(@PathVariable("id") ObjectId id,@RequestBody Bond b) {
        bondService.updateBond(id, b);
    }
    
}
