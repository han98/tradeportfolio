package com.citi.spring.PortfolioApi.data;

import com.citi.spring.PortfolioApi.entities.Bond;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface BondRepository extends MongoRepository<Bond, ObjectId>{
    
}
