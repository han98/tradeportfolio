package com.citi.spring.PortfolioApi.data;

import com.citi.spring.PortfolioApi.entities.Equity;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface EquityRepository extends MongoRepository<Equity, ObjectId> {

}
