package com.citi.spring.PortfolioApi.data;

import com.citi.spring.PortfolioApi.entities.Commodity;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CommodityRepository extends MongoRepository<Commodity, ObjectId> {
    
}
