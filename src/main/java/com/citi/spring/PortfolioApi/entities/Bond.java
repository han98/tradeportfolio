package com.citi.spring.PortfolioApi.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document
public class Bond {

    @Id
    private String _id;
    private Date dateCreated;
    private Date maturityDate;
    private String name;
    private double faceValue;
    private int quantity;
    private int yield;
    private double coupon;
    private String state;

    public Bond(){};

    public Bond(Date dateCreated, Date maturityDate, String name, double faceValue, int quantity, int yield, double coupon){
        this.dateCreated = dateCreated;
        this.maturityDate = maturityDate;
        this.name = name;
        this.faceValue = faceValue;
        this.quantity = quantity;
        this.yield = yield;
        this.coupon = coupon; 
        this.state = "CREATED"; 
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getMaturityDate() {
        return maturityDate;
    }

    public void setMaturityDate(Date maturityDate) {
        this.maturityDate = maturityDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getFaceValue() {
        return faceValue;
    }

    public void setFaceValue(double faceValue) {
        this.faceValue = faceValue;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getYield() {
        return yield;
    }

    public void setYield(int yield) {
        this.yield = yield;
    }

    public double getCoupon() {
        return coupon;
    }

    public void setCoupon(double coupon) {
        this.coupon = coupon;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
    
}
