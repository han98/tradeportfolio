package com.citi.spring.PortfolioApi.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document
public class Equity {

    @Id
    private String _id;
    private Date dateCreated;
    private String ticker;
    private int quantity;
    private String amount;
    private String state;

    public Equity(){}

    public Equity(Date dateCreated, String ticker, int quantity, String amount){
        this.dateCreated = dateCreated;
        this.ticker = ticker;
        this.quantity = quantity;
        this.amount = amount;
        this.state = "CREATED";
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getEquityStatus() {
        return state;
    }

    public void setEquityStatus(String state) {
        this.state = state;
    }


}
