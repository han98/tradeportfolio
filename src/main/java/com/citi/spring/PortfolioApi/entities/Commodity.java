package com.citi.spring.PortfolioApi.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document
public class Commodity {

    @Id
    private String _id;
    private Date dateCreated;
    private String ticker;
    private int quantity;
    private double price;
    private String state;

    public Commodity(){}
    
    public Commodity(Date dateCreated, String ticker, int quantity, double price) {
        this.dateCreated = dateCreated;
        this.ticker = ticker;
        this.quantity = quantity;
        this.price = price;
        this.state = "CREATED";
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    

}