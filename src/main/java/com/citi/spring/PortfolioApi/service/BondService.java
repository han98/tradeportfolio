package com.citi.spring.PortfolioApi.service;

import com.citi.spring.PortfolioApi.data.BondRepository;
import com.citi.spring.PortfolioApi.entities.Bond;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
public class BondService {
    @Autowired
    private BondRepository dao;

    public Collection<Bond> getAll() { return dao.findAll(); }

    public Optional<Bond> getBondById(ObjectId id){
        return dao.findById(id);
    }

    public void deleteBond(ObjectId id){
        dao.deleteById(id);
    }

    public void createBond(Bond b) {
        dao.insert(b);
    }

    public void updateBond(ObjectId id, Bond bond) {
        Optional<Bond> bondData = dao.findById(id);

        if (bondData.isPresent()) {
            Bond updateBond = bondData.get();
            updateBond.setDateCreated(bond.getDateCreated());
            updateBond.setMaturityDate(bond.getMaturityDate());
            updateBond.setFaceValue(bond.getFaceValue());
            updateBond.setQuantity(bond.getQuantity());
            updateBond.setYield(bond.getYield());
            updateBond.setCoupon(bond.getCoupon());
            updateBond.setState(bond.getState());
            dao.save(updateBond);
        }
    }
    
}
