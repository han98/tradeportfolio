package com.citi.spring.PortfolioApi.service;

import com.citi.spring.PortfolioApi.data.CommodityRepository;
import com.citi.spring.PortfolioApi.entities.Commodity;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
public class CommodityService {
    @Autowired
    private CommodityRepository dao;

    public Collection<Commodity> getAll() { return dao.findAll(); }

    public Optional<Commodity> getCommodityById(ObjectId id){
        return dao.findById(id);
	}

    public void deleteCommodity(ObjectId id) {
        dao.deleteById(id);
    }

    public void createCommodity(Commodity c){
        dao.insert(c);
    }

    public void updateCommodity(ObjectId id, Commodity commodity) {
        Optional<Commodity> commodityData = dao.findById(id);

        if (commodityData.isPresent()) {
            Commodity updateCommodity = commodityData.get();
            updateCommodity.setDateCreated(commodity.getDateCreated());
            updateCommodity.setTicker(commodity.getTicker());
            updateCommodity.setQuantity(commodity.getQuantity());
            updateCommodity.setPrice(commodity.getPrice());
            updateCommodity.setState(commodity.getState());
            dao.save(updateCommodity);
        }

    }
    
}
