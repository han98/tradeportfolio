package com.citi.spring.PortfolioApi.service;

import com.citi.spring.PortfolioApi.data.EquityRepository;
import com.citi.spring.PortfolioApi.entities.Equity;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
public class EquityService {
    @Autowired
    private EquityRepository dao;

    public Collection<Equity> getAll() { return dao.findAll(); }

    public Optional<Equity> getEquityById(ObjectId id){
        return dao.findById(id);
	}

    public void deleteEquity(ObjectId id) {
        dao.deleteById(id);
    }

    public void createEquity(Equity t){
        dao.insert(t);
    }

    public void updateEquity(ObjectId id, Equity equity) {
        Optional<Equity> tradeData = dao.findById(id);

        if (tradeData.isPresent()) {
            Equity updateEquity = tradeData.get();
            updateEquity.setDateCreated(equity.getDateCreated());
            updateEquity.setTicker(equity.getTicker());
            updateEquity.setQuantity(equity.getQuantity());
            updateEquity.setAmount(equity.getAmount());
            updateEquity.setEquityStatus(equity.getEquityStatus());
            dao.save(updateEquity);
        }

    }
}
