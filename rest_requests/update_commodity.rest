POST http://localhost:8080/api/trader/{id} HTTP/1.1
content-type: application/json

{
    "dateCreated": "2020-08-28",
    "ticker": "AAPL",
    "quantity": 20,
    "amount": "10.00"
}